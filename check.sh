#!/usr/bin/env bash

show_help() {
cat << EOF
Usage: ${0##*/} [-h] -w WORKSHOP [-l]
  Tool to check for software used during Rip City Software NFJS Workshop
      -h           display this help and exit
      -w WORKSHOP  Check for the required software for workshop WORKSHOP

      WORKSHOP Options are:

        Serverless

      -l           Load docker images for this workshop
EOF
}

load_docker() {

    [[ -z "$WORKSHOP" ]] && \
        echo "Workshop needs to be defined for Load Docker" $$ \
        exit 1

    echo "fetching LocalStack docker image"
    docker pull localstack/localstack:0.10.8

    echo "fetching Swagger UI (now OpenAPI)"
    docker pull swaggerapi/swagger-ui
}

check_app() {
    echo "checking to see if app $1 is installed."
    result=$(hash $1 2>&1)

    if [ ! -z "$result" ]; then
        echo -e "\t$2"
        return 1
    fi
}

check_python_version() {
    # in case the version is sent to stderr instead of stdout
    result=$(python -V | grep '^Python 3')

    [[ $? -ne 0 ]] && \
        echo "Python 3 is required for this workshop"
}

check_serverless() {
    check_app make "The program make is required to use this script!"
    check_app curl "The program curl is required to use this script! Please see https://curl.haxx.se/download.html"
    check_app http "The program http (HTTPie) isn't required but be helpful! Please see https://httpie.org/"
    check_app python "The program python is required for this workshop. Please see https://opensource.com/article/19/5/python-3-default-mac"
    check_python_version
    check_app pipenv "The program pipenv is required for this workshop! Please see https://docs.pipenv.org/en/latest/"
    check_app pip "The program pip is required for this workshop! Please see https://pip.pypa.io/en/stable/installing/"
    check_app aws "The program aws is required for this workshop! Please see https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html"
    check_app docker "The program docker is required for this workshop! Please see https://docs.docker.com/install/"
    check_app code "The program VS Code is required for this workshop! Please see https://code.visualstudio.com/"
    check_app uuidgen "The program uuidgen isn't required but helpful. Please see http://man7.org/linux/man-pages/man1/uuidgen.1.html"
}

WORKSHOP=
DEBUG=
LOAD=

while getopts "hdw:l" opt; do
  case "$opt" in
       h)  show_help; exit 0;;
       d)  DEBUG=1;;
       l)  LOAD=1;;
       w)  WORKSHOP=$OPTARG;;
       \?) show_help >&2; exit 1;;
   esac
done

[[ $DEBUG -eq 1 ]] && set -x

[[ $# -eq 0 ]] && show_help && exit 1

shopt -s nocasematch
[[ "$WORKSHOP" == "serverless" ]] && check_serverless

[[ $LOAD -ne 0 ]] && load_docker